module.exports = {
  extends: 'airbnb',
  env: {
    es6: true,
    browser: true,
  },
  rules: {
    'react/jsx-indent': ['error', 4],
    'react/jsx-filename-extension': [1, {extensions: ['.js', '.jsx']}],
    'jsx-a11y/label-has-for': false,
  },
};
