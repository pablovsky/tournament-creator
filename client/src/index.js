import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Home from './screens/home/index';

const target = document.querySelector('#root');

render(
    <Provider store={store}>
        <Home />
    </Provider>,
    target,
);
