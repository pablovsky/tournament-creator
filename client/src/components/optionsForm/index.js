import React, { Component} from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import Input from './components/input/index';
import { connect } from 'react-redux';
import { newUser } from '../../screens/home/duck/actions';
import optionsForm from '../optionsForm/optionsForm.scss';

class Index extends Component {

    constructor(props){
        super(props);

        this.state = {
            id: '',
            username: ''
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        this.setState({
            id: Math.floor(Math.random() * 1000),
            username: e.target.value,
        })

    }

    onSubmit(e){
        e.preventDefault();
        const {reset} = this.props;
        const user = {
            username: this.state.username,
            id: this.state.id,
        };
        this.props.newUser(user);
        reset();
    }

    render() {
        return (
            <div className={optionsForm.formSettings}>
                <h2>Tournament settings</h2>
                <form onSubmit={this.onSubmit} >
                    <Input
                      onChange={this.onChange}
                    />
                    <br/>
                    <button type="submit" disabled={this.props.pristine || this.props.submitting}>Submit</button>
                    <button type="button" disabled={this.props.submitting} onClick={this.props.reset}>Clear Values</button>
                </form>
            </div>
            )
    }
}

Index.propTypes = {
    newUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    user: state.users.user,
});

export default connect(mapStateToProps, { newUser })(reduxForm({
  form: 'simple',
})(Index));
