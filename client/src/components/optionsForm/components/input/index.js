import React from 'react';
import {Field} from 'redux-form';
import styles from './input.scss';

const required = value => value ? undefined : 'Required';
const minValue = min => value => {
       value && value.length < min ? `Must be at least ${min}` : undefined;
};
const minValue8 = minValue(8);

const renderField = ({ input, label, type, meta: { error} }) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {((error && <span className={styles.field_validation_danger}>{error}</span>))}
        </div>
    </div>
);

const Index = props =>
    (
        <div className={styles.playername}>
            <label>Player name</label>
            <Field validate={[required, minValue8]}
                   onChange={props.onChange}
                   type="text"
                   placeholder="Player name"
                   name="playername"
                   component={renderField}
            />
        </div>
    );


export default Index;
