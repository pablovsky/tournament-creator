import {GET_USERS} from "./types";

export const newUser = (usr) => dispatch => {
    fetch('users/', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(usr)
    })
    .then(res => res.json())
    .then(data =>
        dispatch({
            type: GET_USERS,
            users: data,
        })
    )
};

export const getUsers = () => dispatch => {
    fetch('users' , {
        method: 'GET',
        headers: {
            'content-type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
            dispatch({
                type: GET_USERS,
                users: data,
            })
        }
    )
};
