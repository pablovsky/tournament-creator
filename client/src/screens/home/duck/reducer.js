import {combineReducers} from "redux";
import {reducer as reduxFormReducer} from "redux-form";
import { GET_USERS, NEW_USER } from './types';

const initialState = {
    users: [],
    user: {}
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USERS:
            return {
                ...state,
                users: action.users
            };
        case NEW_USER:
            return {
                ...state,
                user: action.user
            };
        default:
            return state;
    }
};

export default combineReducers({
    form: reduxFormReducer,
    users: usersReducer,
});
