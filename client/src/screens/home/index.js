import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './home.scss';
import showResults from './duck/reducer';
import { getUsers } from './duck/actions';
import SettingsForm from '../../components/optionsForm/index';

class Index extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getUsers();
  }

  render() {
    return (

        <div className={styles.container}>
            <div className={styles.header}>
                <h2>
                      Define tournament settings
                </h2>
            </div>
            <div className={styles.main}>
                <SettingsForm
                  onSubmit={showResults}
                />
                <div className={styles['main--usersList']}>
                    <h1>Users</h1>
                    {this.props.users && this.props.users.length > 1 && this.props.users.map(user =>
                        <div key={user.id}>{user.username}</div>)}
                </div>
            </div>
            <div className={styles.footer}>
                <div className={styles['footer--copyright']}>
                      ©Easy Systems Paweł Kalinowski
                </div>
            </div>
        </div>
    );
  }
}

Index.propTypes = {
  submitting: PropTypes.bool,
  reset: PropTypes.bool,
  users: PropTypes.arrayOf(),
};

Index.defaultProps = {
  submitting: false,
  reset: false,
  users: [],
};

const mapStateToProps = function (state) {
  return {
    users: state.users.users,
  };
};

export default connect(mapStateToProps, { getUsers })(Index);
