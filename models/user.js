const fs = require('fs');
const path = require('path');
const data = require(path.resolve('data', 'users.json'));
exports.findOne = function() {
    return data.users;
};

exports.findAll = function() {
    return data.users;
};

exports.create = function (user) {
    let input = '';
    const readStream = fs.createReadStream(path.resolve('data', 'users.json'), 'utf-8');
    return new Promise((resolve, reject) => {
        readStream.on('data', chunk => {
            input += chunk;
        }).on('end', () => {
            input = JSON.parse(input);
            input.users.push(user);
            const writeStream = fs.createWriteStream(path.resolve('data', 'users.json'));
            writeStream.end(JSON.stringify(input));
            return resolve(input);
        });
    });
};

exports.update = function (id) {

};

exports.delete = function (id) {

};
