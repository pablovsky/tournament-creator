var express = require('express'),
    router = express.Router(),
    user = require('../models/user');

router.post('/', function(req, res, next) {
    const result = user.create(req.body);
    result.then(data => {
        return res.send(data.users);
    }).catch(e => {
        throw new Error('Error');
    });
});

router.get('/', function(req, res) {
    return res.send(user.findAll());
});

router.get('/:id', function(req, res, next) {
    console.log('jest', user.get());
    return res.send(user.findOne(req.params.id));
});

router.put('/:id', function(req, res, next) {
    return res.send(user.update(req.params.id));
});

router.delete('/:id', function(req, res, next) {
    return res.send(user.delete(req.params.id));
});

module.exports = router;
